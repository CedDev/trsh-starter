import * as React from 'react';
import '@styles/components/Button';

interface ButtonProps {
	text?: string,
	action?: ( ()=>void ) | null,
	className?: string
}

const Button:React.SFC<ButtonProps> = ({ text, action, className }) => (
	<span className={`button ${className}`.trim()} onClick={ action === null ? ()=>{} : action }>
		<span className="button-text">{ text }</span>
	</span>
);

Button.defaultProps = {
	text: "",
	action: null,
	className: ""
};

export default Button;
